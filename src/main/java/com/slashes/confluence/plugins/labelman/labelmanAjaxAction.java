package com.slashes.confluence.plugins.labelman;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import com.atlassian.confluence.security.PermissionManager;

abstract class labelmanAjaxAction extends ConfluenceActionSupport implements
		Beanable, ServletRequestAware {
	protected HttpServletRequest httpServletRequest;
	protected LabelManager labelManager;
	protected SpaceManager spaceManager;
	protected PermissionManager permissionManager;

	public String execute() {
		return SUCCESS;
	}

	public abstract Object getBean();

	/**
	 * Dependency-injection of the Confluence LabelManager.
	 */
	public void setLabelManager(LabelManager labelManager) {
		this.labelManager = labelManager;
	}

	/**
	 * Dependency-injection
	 */
	public void setSpaceManager(SpaceManager spaceManager) {
		this.spaceManager = spaceManager;
	}

	/**
	 * Implement ServletRequestAware
	 */
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		this.httpServletRequest = httpServletRequest;
	}

	public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }
}
