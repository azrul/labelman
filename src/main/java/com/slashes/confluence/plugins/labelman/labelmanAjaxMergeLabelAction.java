package com.slashes.confluence.plugins.labelman;

import com.slashes.confluence.plugins.labelman.labelmanAjaxAction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.labels.LabelPermissionSupport;

public class labelmanAjaxMergeLabelAction extends labelmanAjaxAction {
	public String execute() {
        return SUCCESS;
    }
	
	public Object getBean()
    {
		Map<String, Object> bean = new HashMap<String, Object>();
        String[] mergeIds  = httpServletRequest.getParameter("mergeWith").split(",");
        String spaceKey = httpServletRequest.getParameter("spaceKey");
        String labelId = httpServletRequest.getParameter("labelId");
        
        String mergeWithIds = httpServletRequest.getParameter("mergeWith");
        bean.put("mergeWith", mergeWithIds);
        //bean.put("spaceKey", spaceKey);
        
        Label label = labelManager.getLabel(Long.parseLong(labelId));

        List<Label> oldlabels = new ArrayList<Label>();
        for (String item : mergeIds) {
        	Label oldlabel = labelManager.getLabel( Long.parseLong(item));
        	
        	// We can't simply rename a label within a space since it might be shared with other spaces
            // Instead, detach the label from every pages that uses it
            // and attach a new label
            List<Labelable> content =  (List<Labelable>) labelManager.getCurrentContentForLabelAndSpace(oldlabel, spaceKey);
            
            for (Labelable c : content) {
                if (LabelPermissionSupport.userCanEditLabel(label, c, permissionManager) ){
            	   labelManager.removeLabel(c, oldlabel);
            	   labelManager.addLabel(c, label);
                }
            }
        }
        
        return bean;
    }
}
