package com.slashes.confluence.plugins.labelman.breadcrumb;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.breadcrumbs.SpaceBreadcrumb;
import com.atlassian.confluence.util.breadcrumbs.AbstractBreadcrumb;
import com.atlassian.confluence.util.breadcrumbs.Breadcrumb;

public class labelmanBreadcrumb extends AbstractBreadcrumb {

	private final Space space;
	
	public labelmanBreadcrumb(Space space, String title) {
		super(title, null);
        this.space = space;
	}

	@Override
	protected Breadcrumb getParent() {
		return new SpaceBreadcrumb(space);
	}


}
