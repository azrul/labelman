package com.slashes.confluence.plugins.labelman;

import com.slashes.confluence.plugins.labelman.labelmanAjaxAction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.labels.LabelPermissionSupport;

public class labelmanAjaxDeleteLabelAction extends labelmanAjaxAction {
	

    public String execute() {
        return SUCCESS;
    }
	
	public Object getBean()
    {
        Map<String, Object> bean = new HashMap<String, Object>();
        String labelId  = httpServletRequest.getParameter("labelId");
        String spaceKey = httpServletRequest.getParameter("spaceKey");

        bean.put("labelId", labelId);
        bean.put("spaceKey", spaceKey);
 
        Label label = labelManager.getLabel( Long.parseLong(labelId));
        
        // We can't simply delete a label within a space since it might be shared with other spaces
        // Instead, detach the label from every pages that uses it      
        List<Labelable> content =  (List<Labelable>) labelManager.getCurrentContentForLabelAndSpace(label, spaceKey);
        bean.put("affected", content.size());
        for (Labelable item : content) {
            if (LabelPermissionSupport.userCanEditLabel(label, item, permissionManager) ){
        	   labelManager.removeLabel(item, label);
            }
        }

        return bean;
    }

}
