package com.slashes.confluence.plugins.labelman;

import com.slashes.confluence.plugins.labelman.labelmanAjaxAction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelPermissionSupport;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.security.PermissionManager;

public class labelmanAjaxRenameLabelAction extends labelmanAjaxAction {
	

	public String execute() {
		return SUCCESS;
	}

	public Object getBean() {
		Map<String, Object> bean = new HashMap<String, Object>();
		String labelId = httpServletRequest.getParameter("labelId");
		String spaceKey = httpServletRequest.getParameter("spaceKey");
		String newLabelname = httpServletRequest.getParameter("newLabel");

		bean.put("labelId", labelId);
		bean.put("spaceKey", spaceKey);

		Label oldlabel = labelManager.getLabel(Long.parseLong(labelId));
		Label newlabel = new Label(newLabelname, oldlabel.getNamespace());
		
		// We can't simply rename a label within a space since it might be
		// shared with other spaces
		// Instead, detach the label from every pages that uses it
		// and attach a new label
		List<Labelable> content = (List<Labelable>) labelManager
				.getCurrentContentForLabelAndSpace(oldlabel, spaceKey);
		bean.put("affected", content.size());

		for (Labelable item : content) {
			if (LabelPermissionSupport.userCanEditLabel(oldlabel, item, permissionManager) ){
				labelManager.removeLabel(item, oldlabel);
				labelManager.addLabel(item, newlabel);
			}
		}
		
		bean.put("newLabelId" , newlabel.getId());
		bean.put("newLabel" , newlabel.getDisplayTitle());
		
		return bean;
	}
}
