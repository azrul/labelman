package com.slashes.confluence.plugins.labelman;

import java.util.List;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.actions.*;
import com.atlassian.confluence.util.breadcrumbs.Breadcrumb;
import com.atlassian.confluence.util.breadcrumbs.BreadcrumbAware;
import com.atlassian.confluence.util.breadcrumbs.SimpleBreadcrumb;
import com.atlassian.confluence.util.breadcrumbs.SpaceBreadcrumb;
import com.slashes.confluence.plugins.labelman.breadcrumb.*;

public class labelmanPageAction extends ConfluenceActionSupport implements
		SpaceAware, BreadcrumbAware {

	public Space space;
	private SpaceManager spaceManager;
	private LabelManager labelManager;

	@Override
	public boolean isSpaceRequired() {
		return true;
	}

	@Override
	public boolean isViewPermissionRequired() {
		return true;
	}

	@Override
	public boolean isPermitted() {
		return true;
	}

	@Override
	public void setSpace(Space space) {
		this.space = space;
	}


	public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

	/**
	 * Dependency-injection of the Confluence LabelManager.
	 */
	public void setLabelManager(LabelManager labelManager) {
		this.labelManager = labelManager;
	}

	/**
	 * Implementation of PageAware
	 */
	public Space getSpace() {
		return space;
	}

	@Override
	public Breadcrumb getBreadcrumb()
    {
        return new labelmanBreadcrumb(this.space, getText("labelman.title"));
    }

	/**
	 * 
	 * @return
	 */
	public List<Label> getLabels(){
		return this.labelManager.getLabelsInSpace(this.space.getKey()); 
	}
	
	public List<Label> getRecentLabels(){
		return this.labelManager.getRecentlyUsedLabellingsInSpace(this.space.getKey(), 20); 
	}
	
	public List<Label> getPopularLabels(){
		return this.labelManager.getMostPopularLabelsInSpace(this.space.getKey(), 20); 
	}
	
	public String getDisplayTitle() {
		return this.getSpace().getDisplayTitle();
	}

	public String getSpaceKey(){
		return this.getSpace().getKey();
	}
	
	/**
	 * Return the number of pages for the given label
	 * @param l
	 * @return
	 */
	public int getContentCount(Label l)
	{
		return this.labelManager.getContentCount(l);
	}

	public String execute() {
		return SUCCESS;
	}

}