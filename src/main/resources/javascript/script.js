LBM = {
	html : {
		renameLabel : function (oldlabelid, newlabelid, newName){
			// Remove label in sidebar shortcut
			AJS.$('tr[data-label-id]').filter(function(){ 
				return ($(this).data('label-id') == oldlabelid); })
				.data('label-id', newlabelid)
				.find('td[headers=name] span').html(newName);

			AJS.$('a[data-label-id]').filter(function(){ 
				return ($(this).data('label-id') == oldlabelid); })
				.data('label-id', newlabelid)
				.find('span').html(newName);
		},

		mergeLabel : function(labelid) {

		},

		deleteLabel : function(labelid) {

		},

		// Remove the given label from DOM
		labelDeleted : function(labelid) {
			// Remove label in table row
			AJS.$('tr[data-label-id]').filter(function(){ 
				return ($(this).data('label-id') == labelid); })
				.remove();

			// Remove label in sidebar shortcut
			AJS.$('a[data-label-id]').filter(function(){ 
				return ($(this).data('label-id') == labelid); })
				.parents('li').remove();
		}
	}
}

AJS.$(document).ready(function() {

	// Handle label rename
	AJS.$('a[href="#rename"]').live('click', function() {
        
        var labelId = AJS.$(this).parents('tr[data-label-id]').data('label-id');
	    var spaceKey = AJS.$(this).parents('table[data-space-key]').data('space-key');

        var template = labelman.Templates.Rename.renamePopup({spacekey:spaceKey, labelid:labelId});
        var dialogId = "rename-dialog" + labelId;

        var renameDialog = new AJS.Dialog({width:460, height:230, id:dialogId, closeOnOutsideClick: true});
		renameDialog.addHeader("Rename");

		renameDialog.addPanel("Panel 1", template, "panel-body");
		renameDialog.get("panel:0").setPadding(20);

		renameDialog.addButton("Rename", function (dialog) {
		    var newLabelName = AJS.$('form[name=renameForm] input[name="renameNow"]').val();
		    var labelId = AJS.$('form[name=renameForm] input[name="labelid"]').val();
		    var spaceKey = AJS.$('form[name=renameForm] input[name="spacekey"]').val();

		    if(newLabelName.length > 0){
		    	// test for alphanumeric
		    	if(newLabelName.match(/^[a-z0-9]+$/i) != null){

		    		AJS.$('form[name=renameForm]  span.errorMessage').hide();
			        AJS.$.ajax({
			            type: "GET",
			            dataType: "json",
			            url: AJS.params.contextPath + "/spaces/labelman-rename.action?"+
			            	"labelId=" +labelId + 
			            	"&spaceKey=" +spaceKey +
			            	"&newLabel=" + newLabelName,
			            success: function(data) {
			                console.log(data);
			                // Change the label id
			                //AJS.$('tr[data-label-id='+ data.labelId +']').data('label-id', data.newLabelId);
			            	LBM.html.renameLabel(data.labelId, data.newLabelId, data.newLabel);
			            }
			        });
			        dialog.remove();
		    	} else {
		    		// display error warning
		    		AJS.$('form[name=renameForm]  span.errorMessage').show();
		    	}
		    } else {
		    	AJS.$('form[name=renameForm]  span.errorMessage').show();
		    }

		    
		});

		renameDialog.show();
	});

	
	// Handle merge
	AJS.$('a[href="#merge"]').live('click', function() {
        
        var labelId = AJS.$(this).parents('tr[data-label-id]').data('label-id');
	    var spaceKey = AJS.$(this).parents('table[data-space-key]').data('space-key');

	    // Build other labels
	    // Instead of reading it off DOM element, keeping an internal data structure might be
	    // a better option
	    var otherLabels = new Array();
	    AJS.$('tr[data-label-id]').each(function(index) {
	    	var labelname = $(this).find('td[headers=name] span').html();
	    	var labelid = $(this).data('label-id');

	    	// Exclude current selected label
	    	if(labelId != labelid){
        		otherLabels.push( { name: labelname, id: labelid} );
        	}
		});

        var template = labelman.Templates.Rename.mergePopup({spacekey:spaceKey, labelid:labelId, otherLabels: otherLabels });
        var dialogId = "merge-dialog" + labelId;

        var mergeDialog = new AJS.Dialog({width:460, height:330, id:dialogId, closeOnOutsideClick: true});
		mergeDialog.addHeader("Merge");

		mergeDialog.addPanel("Panel 1", template, "panel-body");
		mergeDialog.get("panel:0").setPadding(20);


		mergeDialog.addButton("Merge", function (dialog) {
		    var newLabelName = AJS.$('form[name=mergeForm] input[name="renameNow"]').val();
		    var labelId = AJS.$('form[name=mergeForm] input[name="labelid"]').val();
		    var spaceKey = AJS.$('form[name=mergeForm] input[name="spacekey"]').val();

		    // Build comma-saperated list of label ids
		    var mergeWithArray = new Array();
		    AJS.$("input:checkbox[name=mergewith]:checked").each(function() {
			    mergeWithArray.push($(this).val());
			});
		    var mergeWith = mergeWithArray.join(',');
		    console.log(mergeWith);

		    if(mergeWithArray.length > 0){
		        AJS.$.ajax({
		            type: "GET",
		            dataType: "json",
		            url: AJS.params.contextPath + "/spaces/labelman-merge.action?"+
		            	"labelId=" +labelId + 
		            	"&spaceKey=" +spaceKey +
		            	"&mergeWith=" + mergeWith,
		            success: function(data) {
		                console.log(data);
		                var oldLabels = data.mergeWith.split(',');
		                AJS.$.each(oldLabels, function(index, value){
							LBM.html.labelDeleted(value);
		                });
		            }
		        });
		    }
		    dialog.remove();
		});

		mergeDialog.show();
	});

	// Handle label deletion
	AJS.$('a[href="#delete"]').live('click', function() {
        
	    var labelId = AJS.$(this).parents('tr[data-label-id]').data('label-id');
	    var spaceKey = AJS.$(this).parents('table[data-space-key]').data('space-key');

	        AJS.$.ajax({
	            type: "GET",
	            dataType: "json",
	            url: AJS.params.contextPath + "/spaces/labelman-delete.action?"+
	            	"labelId=" +labelId + 
	            	"&spaceKey=" +spaceKey,
	            success: function(data) {
	                console.log(data);
	                LBM.html.labelDeleted(data.labelId);
	            }
	        });
	    
	});

	// Tag filter
	AJS.$('#searchLabel').live('keyup', function(e){
		e.stopPropagation();
        e.preventDefault();

        AJS.$('tr[data-label-id]').each(function(index) {
        	var lookup = AJS.$('#searchLabel').val();
		    var labelname = $(this).find('td[headers=name] span').html();

		    if(lookup.length == 0 ){
		    	$(this).show();
		    }
		    else if( labelname.indexOf(lookup) > 0 ) {
		    	$(this).show();
		    } else {
		    	$(this).hide();
		    }
		});
        
	});

	// Filter when user click the sidebar item
	AJS.$('ul.label-list a[rel=tag]').live('click', function(){
		AJS.$('#searchLabel').val( AJS.$(this).find('span').html() );
	});
});